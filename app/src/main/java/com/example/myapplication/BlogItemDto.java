package com.example.myapplication;

import java.io.Serializable;

public class BlogItemDto implements  Serializable  {
    String name;
    String country;
    int image;

    public BlogItemDto(String name, String country, int image) {
        this.name = name;
        this.country = country;
        this.image = image;
    }
}
