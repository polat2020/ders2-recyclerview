package com.example.myapplication;

import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.databinding.RecyclerRowBinding;

public class BlogHolder extends RecyclerView.ViewHolder {

    RecyclerRowBinding binding;

    public BlogHolder(RecyclerRowBinding recyclerRowBinding) {
        super(recyclerRowBinding.getRoot());
        this.binding = recyclerRowBinding;
    }
}
