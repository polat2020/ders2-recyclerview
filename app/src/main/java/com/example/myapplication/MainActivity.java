package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.example.myapplication.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<BlogItemDto> list;
    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        this.Getir();

    }

    public void Getir() {
        list = new ArrayList<>();
        list.add(new BlogItemDto("lays", "Tr", R.drawable.item1));
        list.add(new BlogItemDto("twitter", "Tr", R.drawable.twitter));
        list.add(new BlogItemDto("turkcell", "Tr", R.drawable.turkcell));

        ArrayList<String> accumulator = new ArrayList<>();
        for (BlogItemDto item : list)
            accumulator.add(item.name);

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1,
                accumulator
        );


        binding.listView1.setAdapter(arrayAdapter);

        binding.listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // Toast.makeText(MainActivity.this,list.get(i).name,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this,
                        DetailActivity.class);

                intent.putExtra("item", list.get(i));

                startActivity(intent);

            }
        });
    }

}