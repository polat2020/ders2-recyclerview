package com.example.myapplication;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.databinding.RecyclerRowBinding;

import java.util.ArrayList;

public class BlogAdapter extends RecyclerView.Adapter<BlogHolder> {

    ArrayList<BlogItemDto> list;

    public BlogAdapter(ArrayList<BlogItemDto> list) {
        this.list = list;
    }


    @NonNull
    @Override
    public BlogHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerRowBinding recyclerRowBinding= RecyclerRowBinding.inflate(LayoutInflater.from(parent.getContext()),parent, false);
        return  new BlogHolder(recyclerRowBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BlogHolder holder, int position) {

        holder.binding.txtName.setText(list.get(position).name);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(holder.itemView.getContext(), DetailActivity.class);

                intent.putExtra("item", list.get(position));
                holder.itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();

    }
}
