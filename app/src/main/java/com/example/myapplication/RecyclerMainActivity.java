package com.example.myapplication;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.databinding.ActivityRecyclerMainBinding;

import java.util.ArrayList;

public class RecyclerMainActivity extends AppCompatActivity {

    ArrayList<BlogItemDto> list;
    ActivityRecyclerMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRecyclerMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        list = new ArrayList<>();
        list.add(new BlogItemDto("lays", "Tr", R.drawable.item1));
        list.add(new BlogItemDto("twitter", "Tr", R.drawable.twitter));
        list.add(new BlogItemDto("turkcell", "Tr", R.drawable.turkcell));

        ArrayList<String> accumulator = new ArrayList<>();
        for (BlogItemDto item : list)
            accumulator.add(item.name);

        BlogAdapter blogAdapter = new BlogAdapter(list);
        binding.recyclerView.setAdapter(blogAdapter);


    }
}