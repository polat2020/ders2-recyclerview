package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.databinding.ActivityDetailBinding;

public class DetailActivity extends AppCompatActivity {

    ActivityDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetailBinding.inflate(getLayoutInflater());
        View view= binding.getRoot();
        setContentView(view);

        Intent intent = getIntent();

        BlogItemDto selectedItem = (BlogItemDto) intent.getSerializableExtra("list");

        binding.txtName.setText(selectedItem.name);
        binding.txtCountry.setText(selectedItem.country);
        binding.img1.setImageResource(selectedItem.image);

    }
}